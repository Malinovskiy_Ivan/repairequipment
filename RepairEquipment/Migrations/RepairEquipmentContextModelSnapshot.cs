﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ServiceCenter.Migrations
{
    [DbContext(typeof(RepairEquipmentContextModelSnapshot))]
    partial class RepairEquipmentContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("RepairEquipment.Models.Customer", b =>
                {
                    b.Property<int>("CustomerId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("FullName");

                    b.Property<string>("HomeAddress");

                    b.Property<string>("PhoneNumber");

                    b.HasKey("CustomerId");

                    b.ToTable("Customers");
                });

            modelBuilder.Entity("RepairEquipment.Models.Employee", b =>
                {
                    b.Property<int>("EmployeeId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("FullName");

                    b.Property<string>("PhoneNumber");

                    b.HasKey("EmployeeId");

                    b.ToTable("Employees");
                });

            modelBuilder.Entity("RepairEquipment.Models.Malfunction", b =>
                {
                    b.Property<int>("MalfunctionId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.Property<int?>("OrderId");

                    b.Property<string>("RepairMethods");

                    b.Property<int>("WorkPrice");

                    b.HasKey("MalfunctionId");

                    b.HasIndex("OrderId");

                    b.ToTable("Malfunctions");
                });

            modelBuilder.Entity("RepairEquipment.Models.Model", b =>
                {
                    b.Property<int>("ModelId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Features");

                    b.Property<string>("Manufacturer");

                    b.Property<string>("ModelType");

                    b.Property<string>("Name");

                    b.Property<string>("Specifications");

                    b.Property<string>("Store");

                    b.HasKey("ModelId");

                    b.ToTable("Models");
                });

            modelBuilder.Entity("RepairEquipment.Models.Order", b =>
                {
                    b.Property<int>("OrderId")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("CustomerId");

                    b.Property<int?>("EmployeeId");

                    b.Property<int?>("ModelId");

                    b.Property<DateTime>("OrderDate");

                    b.Property<DateTime>("ReturnDate");

                    b.Property<string>("SerialNumber");

                    b.Property<string>("WarrantyNote");

                    b.Property<DateTime>("WarrantyRepairPeriod");

                    b.HasKey("OrderId");

                    b.HasIndex("CustomerId");

                    b.HasIndex("EmployeeId");

                    b.HasIndex("ModelId");

                    b.ToTable("Orders");
                });

            modelBuilder.Entity("RepairEquipment.Models.Spare", b =>
                {
                    b.Property<int>("SpareId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Functions");

                    b.Property<string>("Name");

                    b.Property<int?>("OrderId");

                    b.Property<int>("Price");

                    b.HasKey("SpareId");

                    b.HasIndex("OrderId");

                    b.ToTable("Spares");
                });

            modelBuilder.Entity("RepairEquipment.Models.Malfunction", b =>
                {
                    b.HasOne("RepairEquipment.Models.Order", "Order")
                        .WithMany("Malfunctions")
                        .HasForeignKey("OrderId");
                });

            modelBuilder.Entity("RepairEquipment.Models.Order", b =>
                {
                    b.HasOne("RepairEquipment.Models.Customer", "Customer")
                        .WithMany("Orders")
                        .HasForeignKey("CustomerId");

                    b.HasOne("RepairEquipment.Models.Employee", "Employee")
                        .WithMany("Orders")
                        .HasForeignKey("EmployeeId");

                    b.HasOne("RepairEquipment.Models.Model", "Model")
                        .WithMany("Orders")
                        .HasForeignKey("ModelId");
                });

            modelBuilder.Entity("RepairEquipment.Models.Spare", b =>
                {
                    b.HasOne("RepairEquipment.Models.Order", "Order")
                        .WithMany("Spares")
                        .HasForeignKey("OrderId");
                });
        }
    }
}
