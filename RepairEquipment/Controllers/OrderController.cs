using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RepairEquipment.Models;
using PagedList.Core;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace RepairEquipment.Controllers
{
    [Authorize]
    public class OrderController : Controller
    {
        RepairEquipmentContext db;

        public OrderController(RepairEquipmentContext context)
        {
            db = context;
        }

        // GET: Order
        public ActionResult Index(int? page, string searchString)
        {
            int pageSize = 10;
            int pageNumber = (page ?? 1);

            var order = db.Orders.OrderBy(t => t.OrderId).Include(t => t.Employee).Include(t => t.Model).Include(t => t.Customer).ToPagedList(pageNumber, pageSize);

            if (!String.IsNullOrEmpty(searchString))
            {
                order = db.Orders.Where(t => t.SerialNumber.Contains(searchString)).OrderBy(t => t.OrderId).Include(t => t.Employee).Include(t => t.Model).Include(t => t.Customer).ToPagedList(pageNumber, pageSize);
            }

            return View(order);
        }

        // GET: Order/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new StatusCodeResult(StatusCodes.Status400BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return NotFound();
            }
            return View(order);
        }

        // GET: Order/Create
        public ActionResult Create()
        {
            ViewBag.ModelId = new SelectList(db.Models, "ModelId", "Name");
            ViewBag.EmployeeId = new SelectList(db.Employees, "EmployeeId", "FullName");
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "FullName");
            return View();
        }

        // POST: Order/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind("OrderId", "OrderDate", "SerialNumber", "ReturnDate", "WarrantyNote", "WarrantyRepairPeriod", "EmployeeId", "ModelId", "CustomerId")] Order order)
        {
            if (ModelState.IsValid)
            {
                db.Orders.Add(order);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ModelId = new SelectList(db.Models, "ModelId", "Name", order.ModelId);
            ViewBag.EmployeeId = new SelectList(db.Employees, "EmployeeId", "FullName", order.EmployeeId);
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "FullName", order.CustomerId);
            return View(order);
        }

        // GET: Order/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new StatusCodeResult(StatusCodes.Status400BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return NotFound();
            }
            ViewBag.ModelId = new SelectList(db.Models, "ModelId", "Name", order.ModelId);
            ViewBag.EmployeeId = new SelectList(db.Employees, "EmployeeId", "FullName", order.EmployeeId);
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "FullName", order.CustomerId);
            return View(order);
        }

        // POST: Order/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind("OrderId", "OrderDate", "SerialNumber", "ReturnDate", "WarrantyNote", "WarrantyRepairPeriod", "EmployeeId", "ModelId", "CustomerId")] Order order)
        {
            if (ModelState.IsValid)
            {
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ModelId = new SelectList(db.Models, "ModelId", "Name", order.ModelId);
            ViewBag.EmployeeId = new SelectList(db.Employees, "EmployeeId", "FullName", order.EmployeeId);
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "FullName", order.CustomerId);
            return View(order);
        }

        // GET: Order/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new StatusCodeResult(StatusCodes.Status400BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return NotFound();
            }
            return View(order);
        }

        // POST: Order/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            Order order = db.Orders.Find(id);
            db.Orders.Remove(order);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}