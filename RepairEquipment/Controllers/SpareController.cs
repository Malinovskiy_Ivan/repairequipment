using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RepairEquipment.Models;
using PagedList.Core;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace RepairEquipment.Controllers
{
    [Authorize]
    public class SpareController : Controller
    {
        RepairEquipmentContext db;

        public SpareController(RepairEquipmentContext context)
        {
            db = context;
        }

        // GET: Spare
        public ActionResult Index(int? page, string searchString)
        {
            int pageSize = 10;
            int pageNumber = (page ?? 1);

            var spare = db.Spares.OrderBy(t => t.SpareId).Include(t => t.Order).ToPagedList(pageNumber, pageSize);

            if (!String.IsNullOrEmpty(searchString))
            {
                spare = db.Spares.Where(t => t.Name.Contains(searchString)).OrderBy(t => t.SpareId).Include(t => t.Order).ToPagedList(pageNumber, pageSize);
            }

            return View(spare);
        }

        // GET: Spare/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new StatusCodeResult(StatusCodes.Status400BadRequest);
            }
            Spare spare = db.Spares.Find(id);
            if (spare == null)
            {
                return NotFound();
            }
            return View(spare);
        }

        // GET: Spare/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Spare/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind("SpareId", "OrderId", "Name", "Functions", "Price")] Spare spare)
        {
            if (ModelState.IsValid)
            {
                db.Spares.Add(spare);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(spare);
        }

        // GET: Spare/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new StatusCodeResult(StatusCodes.Status400BadRequest);
            }
            Spare spare = db.Spares.Find(id);
            if (spare == null)
            {
                return NotFound();
            }
            return View(spare);
        }

        // POST: Spare/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind("SpareId", "OrderId", "Name", "Functions", "Price")] Spare spare)
        {
            if (ModelState.IsValid)
            {
                db.Entry(spare).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(spare);
        }

        // GET: Spare/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new StatusCodeResult(StatusCodes.Status400BadRequest);
            }
            Spare spare = db.Spares.Find(id);
            if (spare == null)
            {
                return NotFound();
            }
            return View(spare);
        }

        // POST: Spare/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            Spare spare = db.Spares.Find(id);
            db.Spares.Remove(spare);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}