using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RepairEquipment.Models;
using PagedList.Core;
using Microsoft.AspNetCore.Authorization;

namespace RepairEquipment.Controllers
{
    [Authorize]
    public class EmployeeController : Controller
    {
        RepairEquipmentContext db;

        public EmployeeController(RepairEquipmentContext context)
        {
            db = context;
        }

        // GET: Employee
        public ActionResult Index(int? page, string searchString)
        {
            int pageSize = 10;
            int pageNumber = (page ?? 1);

            var employee = db.Employees.OrderBy(t => t.EmployeeId).ToPagedList(pageNumber, pageSize);

            if (!String.IsNullOrEmpty(searchString))
            {
                employee = db.Employees.Where(t => t.FullName.Contains(searchString)).OrderBy(t => t.EmployeeId).ToPagedList(pageNumber, pageSize);
            }

            return View(employee);
        }

        // GET: Employee/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new StatusCodeResult(StatusCodes.Status400BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return NotFound();
            }
            return View(employee);
        }

        // GET: Employee/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Employee/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Employee/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Employee/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Employee/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Employee/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}