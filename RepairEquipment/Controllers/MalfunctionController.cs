using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RepairEquipment.Models;
using PagedList.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace RepairEquipment.Controllers
{
    [Authorize]
    public class MalfunctionController : Controller
    {
        RepairEquipmentContext db;

        public MalfunctionController(RepairEquipmentContext context)
        {
            db = context;
        }

        // GET: Malfunction
        public ActionResult Index(int? page, string searchString)
        {
            int pageSize = 10;
            int pageNumber = (page ?? 1);

            var malfunction = db.Malfunctions.OrderBy(t => t.MalfunctionId).Include(t => t.Order).ToPagedList(pageNumber, pageSize);

            if (!String.IsNullOrEmpty(searchString))
            {
                malfunction = db.Malfunctions.Where(t => t.Name.Contains(searchString)).OrderBy(t => t.MalfunctionId).Include(t => t.Order).ToPagedList(pageNumber, pageSize);
            }

            return View(malfunction);
        }

        // GET: Malfunction/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new StatusCodeResult(StatusCodes.Status400BadRequest);
            }
            Malfunction malfunction = db.Malfunctions.Find(id);
            if (malfunction == null)
            {
                return NotFound();
            }
            return View(malfunction);
        }

        // GET: Malfunction/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Malfunction/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind("MalfunctionId","OrderId","Name","RepairMethods","WorkPrice")] Malfunction malfunction)
        {
            if (ModelState.IsValid)
            {
                db.Malfunctions.Add(malfunction);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(malfunction);
        }

        // GET: Malfunction/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new StatusCodeResult(StatusCodes.Status400BadRequest);
            }
            Malfunction malfunction = db.Malfunctions.Find(id);
            if (malfunction == null)
            {
                return NotFound();
            }
            return View(malfunction);
        }

        // POST: Malfunction/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind("MalfunctionId", "OrderId", "Name", "RepairMethods", "WorkPrice")] Malfunction malfunction)
        {
            if (ModelState.IsValid)
            {
                db.Entry(malfunction).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(malfunction);
        }

        // GET: Malfunction/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new StatusCodeResult(StatusCodes.Status400BadRequest);
            }
            Malfunction malfunction = db.Malfunctions.Find(id);
            if (malfunction == null)
            {
                return NotFound();
            }
            return View(malfunction);
        }

        // POST: Malfunction/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            Malfunction malfunction = db.Malfunctions.Find(id);
            db.Malfunctions.Remove(malfunction);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}