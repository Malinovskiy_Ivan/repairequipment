using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RepairEquipment.Models;
using PagedList.Core;
using Microsoft.AspNetCore.Authorization;

namespace RepairEquipment.Controllers
{
    [Authorize]
    public class ModelController : Controller
    {
        RepairEquipmentContext db;

        public ModelController(RepairEquipmentContext context)
        {
            db = context;
        }

        // GET: Model
        public ActionResult Index(int? page, string searchString)
        {
            int pageSize = 10;
            int pageNumber = (page ?? 1);

            var model = db.Models.OrderBy(t => t.ModelId).ToPagedList(pageNumber, pageSize);

            if (!String.IsNullOrEmpty(searchString))
            {
                model = db.Models.Where(t => t.Name.Contains(searchString)).OrderBy(t => t.ModelId).ToPagedList(pageNumber, pageSize);
            }

            return View(model);
        }

        // GET: Model/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new StatusCodeResult(StatusCodes.Status400BadRequest);
            }
            Model model = db.Models.Find(id);
            if (model == null)
            {
                return NotFound();
            }
            return View(model);
        }

        // GET: Model/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Model/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Model/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Model/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Model/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Model/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}