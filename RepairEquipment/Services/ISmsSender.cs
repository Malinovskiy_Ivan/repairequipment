﻿using System.Threading.Tasks;

namespace RepairEquipment.Services
{
    public interface ISmsSender
    {
        Task SendSmsAsync(string number, string message);
    }
}
