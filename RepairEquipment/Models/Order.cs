﻿using System;
using System.Collections.Generic;

namespace RepairEquipment.Models
{
    public class Order
    {
        public int OrderId { get; set; }
        public DateTime OrderDate { get; set; }
        public string SerialNumber { get; set; }
        public DateTime ReturnDate { get; set; }
        public string WarrantyNote { get; set; }
        public DateTime WarrantyRepairPeriod { get; set; }
        public int EmployeeId { get; set; }
        public int ModelId { get; set; }
        public int CustomerId { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual Model Model { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual ICollection<Spare> Spares { get; set; }
        public virtual ICollection<Malfunction> Malfunctions { get; set; }

        public Order()
        {
            Spares = new List<Spare>();
            Malfunctions = new List<Malfunction>();
        }
    }
}