﻿using System.Collections.Generic;

namespace RepairEquipment.Models
{
    public class Model
    {
        public int ModelId { get; set; }
        public string Name { get; set; }
        public string ModelType { get; set; }
        public string Manufacturer { get; set; }
        public string Specifications { get; set; }
        public string Features { get; set; }
        public string Store { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
        public Model()
        {
            Orders = new List<Order>();
        }
    }
}