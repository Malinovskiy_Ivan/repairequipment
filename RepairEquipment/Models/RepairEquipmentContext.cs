﻿using Microsoft.EntityFrameworkCore;

namespace RepairEquipment.Models
{
    public class RepairEquipmentContext : DbContext
    {
        public RepairEquipmentContext(DbContextOptions<RepairEquipmentContext> options) : base(options)
        {

        }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Model> Models { get; set; }
        public DbSet<Spare> Spares { get; set; }
        public DbSet<Malfunction> Malfunctions { get; set; }

    }
}