﻿using System.ComponentModel.DataAnnotations;

namespace RepairEquipment.Models
{
    public class Spare
    {
        [Key]
        public int SpareId { get; set; }
        public int OrderId { get; set; }
        public string Name { get; set; }
        public string Functions { get; set; }
        public int Price { get; set; }

        public virtual Order Order { get; set; }
    }
}
