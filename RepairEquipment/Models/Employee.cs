﻿using System.Collections.Generic;

namespace RepairEquipment.Models
{
    public class Employee
    {
        public int EmployeeId { get; set; }
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
        public Employee()
        {
            Orders = new List<Order>();
        }
    }
}