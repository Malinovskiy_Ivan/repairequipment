﻿using System.ComponentModel.DataAnnotations;

namespace RepairEquipment.Models
{
    public class Malfunction
    {
        [Key]
        public int MalfunctionId { get; set; }
        public int OrderId { get; set; }
        public string Name { get; set; }
        public string RepairMethods { get; set; }
        public int WorkPrice { get; set; }

        public virtual Order Order { get; set; }
    }
}
